
# Parent class
class Person():
    
    def __init__(self, name, age):
        self.name = name
        self.age = age
        
    def run(self):
        print(self.name + " can run.")
        
    def speak(self):
        print(self.name + " can speak.")
        
    def perosnAge(self):
        print("Age is " + str(self.age) + ".")
        
# Child class
class Man(Person):
    
    def __init__(self, name, age, address):
        super().__init__(name, age)
        self.address = address
        
    def perosnAddress(self):
        print("Address is " + self.address + ".")
        
manObject = Man("Yogesh", 31, "D-103, Royal Entrada")
manObject.run()
manObject.speak()
manObject.perosnAge()
manObject.perosnAddress()
