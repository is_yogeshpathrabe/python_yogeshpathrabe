"""
Addition of string(higher) data type and integer(lower) data type
"""

class Addition:
    
    def __init__(self, myString, myInteger):
        self.myString = myString
        self.myInteger = myInteger

objectAddition = Addition("1988", 32)

print("\nmyString data type is", type(objectAddition.myString), "and myInteger data type is", type(objectAddition.myInteger))

# Doing type casting
myIntegerString = int(objectAddition.myString)
print("\nmyIntegerString data type is", type(myIntegerString))

myAddition = myIntegerString + objectAddition.myInteger

print("\nThe addition of string(higher) data type with value " + objectAddition.myString + \
      " and integer(lower) data type with value", objectAddition.myInteger, \
      "is", myAddition)
