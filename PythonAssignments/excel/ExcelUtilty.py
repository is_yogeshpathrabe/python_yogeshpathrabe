
import pandas
import os
from util.FileUtility import getResourcesPath

trackingDetailsFilePath = os.path.join(getResourcesPath(), 'TrackingDetails.xlsx')

class ExcelUtility:

    def getFile(self, filePath, sheetName):
        return pandas.read_excel(filePath, sheet_name=sheetName)
    
    def isEmployeePresent(self, fileData, employeeName, organizationName):
        for index in fileData.index:
            currentEmployee = fileData['Name'][index]
            currentOrganization = fileData['Organization Name'][index]
            if currentEmployee == employeeName and currentOrganization == organizationName:
                return index
        return -1
    
    def isOrganizationPresent(self, fileData, organizationName):
        for index in fileData.index:
            currentOrganization = fileData['Organization Name'][index]
            if currentOrganization == organizationName:
                return index
        return -1

# Main method
if __name__ == '__main__':
    
    ''' This will print columns from sheet '''
    #print(ExcelUtility().getEmployeesData().columns)
    
    ''' This will print column each row data '''
    #print(ExcelUtility().getEmployeesData()['Name'])
    
    # print(ExcelUtility().getOrganizationsData())
    
    ''' To iterate over the list we can use a loop: '''
    employeesData = ExcelUtility().getFile(trackingDetailsFilePath, "Employees")
    # for index in employeesData.index:
        # print(employeesData['Job Title'][index])
    
    print(ExcelUtility().isEmployeePresent(employeesData, "Yogesh Pathrabe", "Google"))
