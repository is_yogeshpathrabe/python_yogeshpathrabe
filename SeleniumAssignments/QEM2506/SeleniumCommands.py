

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.utils import ChromeType
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.touch_actions import TouchActions

class SeleniumCommands:

    def test_sample(self):

        driver = webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install())
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")

        # Find element command
        element = driver.find_element_by_class_name('test')
        element.click()

        # By command with find element
        by_element = driver.find_element(By.CSS_SELECTOR, '#test')
        by_element.send_keys("Yogesh")

        # Multiple elements
        multiple_elements = driver.find_elements(By.CSS_SELECTOR, '#test')
        for element in multiple_elements:
            print(element.text)

        # Select box command
        select_box = Select(driver.find_element_by_css_selector('.test'))
        select_box.select_by_visible_text('test')

        # Implicit wait of 60 seconds
        driver.implicitly_wait(60)

        # Explicit wait of 60 seconds
        wait = WebDriverWait(driver, 60)
        search_bar = wait.until(ec.presence_of_element_located((By.XPATH, "//div[@class='test']")))

        # Screenshot default available
        driver.save_screenshot('<path>/test.png')

        # Alert handling
        alert = driver.switch_to.alert
        alert.accept()

        # Alert via Alert class
        alert = Alert(driver)
        alert.dismiss()

        # Touch actions
        touch_action = TouchActions(driver)
        touch_action.double_tap(element)
        touch_action.scroll(10, 20)

        # Open empty tab
        driver.execute_script('''window.open("https://www.google.co.in/","_blank")''')

        # Window handle
        window = driver.window_handles[0]
        driver.switch_to.window(window)

        # Assertion using assert keyword
        assert "Demo" in driver.title

        # Close all opened browsers
        driver.quit()
