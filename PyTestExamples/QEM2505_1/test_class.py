
# content of test_class.py
class TestClass:
    
    def test_one(self):
        x = "this"
        assert "h" in x

    def test_two(self):
        x = "hello"
        assert hasattr(x, "check")

# Run the file using below command in command prompt -
# pytest -q test_class.py

class TestClassDemoInstance:
    
    def test_one(self):
        assert 0

    def test_two(self):
        assert 0

# Run the file using below command in command prompt -
# pytest -k TestClassDemoInstance -q test_class.py
