
# content of test_sample.py
def func(x):
    return x + 1


def test_answer():
    assert func(3) == 5

# Run the file using below command in command prompt -
# pytest
