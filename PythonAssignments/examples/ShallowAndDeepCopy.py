
""" Copy an Object in Python """
# Usually we are using = (equal sign) and both the variables have same reference
# So, if value change for any variable will reflect to both the variables

old_list = [[1, 2, 3], [4, 5, 6], [7, 8, 'a']]

print('Old List:', old_list)

new_list = old_list

new_list[2][2] = 9

print('Updated Old List:', old_list)
print('Id of Old List:', id(old_list))

print('New List:', new_list)
print('Id of New List:', id(new_list))

import copy

""" Shallow Copy """
print("\nShallow Copy")
# A shallow copy creates a new object which stores the reference of the original elements
# Shallow copy doesn't create a copy of nested objects, instead it just copies the reference of nested objects.
# This means, a copy process does not recurse or create copies of nested objects itself

old_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
new_list = copy.copy(old_list)

print("Example1,\nOld list:", old_list)
print("New list:", new_list)

old_list = [[1, 1, 1], [2, 2, 2], [3, 3, 3]]
new_list = copy.copy(old_list)

old_list.append([4, 4, 4])

print("Example2,\nOld list:", old_list)
print("New list:", new_list)

old_list = [[1, 1, 1], [2, 2, 2], [3, 3, 3]]
new_list = copy.copy(old_list)

old_list[1][1] = 'AA'

print("Example3,\nOld list:", old_list)
print("New list:", new_list)

""" Deep Copy """
print("\nDeep Copy")
# A deep copy creates a new object and recursively adds the copies of nested objects present in the original elements.
# The deep copy creates independent copy of original object and all its nested objects.

old_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
new_list = copy.deepcopy(old_list)

print("Example1,\nOld list:", old_list)
print("New list:", new_list)

old_list = [[1, 1, 1], [2, 2, 2], [3, 3, 3]]
new_list = copy.deepcopy(old_list)

old_list.append([4, 4, 4])

print("Example2,\nOld list:", old_list)
print("New list:", new_list)

old_list = [[1, 1, 1], [2, 2, 2], [3, 3, 3]]
new_list = copy.deepcopy(old_list)

old_list[1][1] = 'AA'

print("Example3,\nOld list:", old_list)
print("New list:", new_list)
