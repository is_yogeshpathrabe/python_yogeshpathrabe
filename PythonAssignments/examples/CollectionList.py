# List
""" Ordered|Indexed|Changeable|Duplicates """
""" Syntax: [] """

listA = ["A", "B", "C", "D", "E"]
print("Main list", listA)
print("\n2nd index value " + listA[2] + "\n")

""" Changing the value """
listA[2] = "C1"
print("Modified list", listA, "\n")

""" For loop """
print("For loop printing")
for value in listA:
    print(value)

print("\nList size ", len(listA), "\n")

""" Operations """
listA.append("Z")
print("Appended list", listA)

listA.insert(1, "X")
print("\nInserted list", listA)

listA.remove("X") #listA.remove(listA[1])
print("\nRemoved list", listA)

listA.pop()
print("\nPop list", listA)

listA.pop(1)
print("\nPop index list", listA)

del listA[0]
print("\nDeleted list", listA)

listA.reverse()
print("\nReverse list", listA)

listA.clear()
print("\nCleared list", listA)

""" Multiple data type list """
listB = ["Yogesh", 1, 2, 2.5]
print("\Multiple data type list", listB)

""" Nested list """
listC = ["Yogesh", [1, 2, 3], ['x', 'y', 'z']]
print("\nNested list", listC[2][1])
