# Dictionary
""" Unordered|Indexed|Changeable|No Duplicates """
""" Syntax: {K:V} where K = Key and V = Value"""

dictionaryA = {
        "firstName":"Yogesh",
        "lastName":"Pathrabe",
        "age":31,
        "dateOfBirth":"25-06-1988",
        "employeeId":1287
    }

print("Main dictionary", dictionaryA)

# Get value using index in which index is key
print("\nEmployee Id is", dictionaryA["employeeId"])

# Get value using get method in which parameter is key
print("\nDate of birth is", dictionaryA.get("dateOfBirth"))

# If no key present then it will print default value
print("\nDesignation is", dictionaryA.get("designation", "Senior Technical Lead (Automation)"))

# It will print all values
print("\nValues are", dictionaryA.values())

""" For loop - It will print only keys """
print("For loop - It will print only keys")
for key in dictionaryA:
    print(key)

""" For loop - It will print values """
print("\nFor loop - It will print values")
for key in dictionaryA:
    print(dictionaryA[key])

""" For loop - It will print both keys and values """
print("\nFor loop - It will print both keys and values")
for key,vlaue in dictionaryA.items():
    print(key, "is", vlaue)

""" Changing the value """
dictionaryA["age"] = 30
print("Modified dictionary", dictionaryA)

""" Adding the new key and value """
dictionaryA["designation"] = "Senior Technical Lead (Automation)"
print("New dictionary", dictionaryA)

""" Delete provided key """
dictionaryA.pop("age")
print("\nPop dictionary", dictionaryA)

""" Delete last key """
dictionaryA.popitem()
print("\nPopitem dictionary", dictionaryA)

""" Delete provided key """
del dictionaryA["dateOfBirth"]
print("\nDelete dictionary", dictionaryA)

""" Clear dictionary keys and values """
dictionaryA.clear()
print("\nClear dictionary", dictionaryA)

""" Delete complete dictionary """
del dictionaryA
