
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.utils import ChromeType
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import IEDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.opera import OperaDriverManager
from selenium import webdriver


class TestWebDriverManager:

    def test_chrome(self):
        driver = webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install())
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")
        driver.quit()

    def test_firefox(self):
        driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")
        driver.quit()

    def test_ie(self):
        driver = webdriver.Ie(IEDriverManager().install())
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")
        driver.quit()

    def test_edge(self):
        driver = webdriver.Edge(EdgeChromiumDriverManager().install())
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")
        driver.quit()

    def test_opera(self):
        driver = webdriver.Opera(executable_path=OperaDriverManager().install())
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")
        driver.quit()
