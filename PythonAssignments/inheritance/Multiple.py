
# Grand parent class
class GrandParent:
    
    def family(self):
        print("I am grand parent")
        
# Parent class
class Father(GrandParent):
    
    fatherName = ""
    
    def father(self):
        print("I am Father, my name is " + self.fatherName)

# Parent class
class Mother(GrandParent):
    
    motherName = ""
    
    def mother(self):
        print("I am Mother, my name is " + self.motherName)
        
# Child class
class Son(Father, Mother):
    
    sonName = ""
    
    def son(self):
        print("My Father is " + self.fatherName)
        print("My Mother is " + self.motherName)

sonObject = Son()
sonObject.fatherName = "Atmaram"
sonObject.motherName = "Anandi"

sonObject.family()
sonObject.son()
sonObject.father()
sonObject.mother()
