
import unittest

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

'''
Make sure selenium server should be connect using below command:
D:\Softwares\Selenium Server>java -Dwebdriver.chrome.driver=chromedriver.exe -jar selenium-server-standalone-3.141.59.jar
'''

class UnitTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME)
        '''
        driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities={'browserName': 'htmlunit', 'version': '2', 'javascriptEnabled': True})
        '''
        self.driver.delete_all_cookies()
        self.driver.maximize_window()
        self.driver.get("https://www.google.co.in/")

    def test_sample(self):
        self.assertEqual(self.driver.title, "Google")

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()
