# Variables
print("Enter any number: ")
intX = int(input())
intY = 10

# If Else
if intX > intY:
    print(str(intX) + " is greater than " + str(intY))
elif intX < intY:
    print(str(intX) + " is less than " + str(intY))
else:
    print(str(intX) + " is equal to " + str(intY))

# For Loop
myIntArrayList = [10, 11, 12, 13, 14]
myStringArrayList = ["A", "B", "C", "D", "E"]

print()
for integerValue in myIntArrayList:
    print("My integer", integerValue)

print()
for stringValue in myStringArrayList:
    print("My string", stringValue)
else:
    print("No string present")

# While Loop
intY = 0
intSum = 0

print()
while intX > intY:
    intSum = intSum + intX
    intY = intY + 1;
print("Calculation done and value is", intSum)

print("\nEnter any number: ")
intX = int(input())
intY = 0
intSum = 0
while intY < intX:
    intSum = intSum + intX
    intY = intY + 1;
else:
    print("Calculation done and value is", intSum)
