
try:
    print(5/0)
except ZeroDivisionError:
    print("ZeroDivisionError occurred")
except:
    print("Exception occurred")
finally:
    print("Inside finally block")

print("\nElse block: \n")

# We can use else as well
# If everything is fine then else will execute
try:
    print("All good")
except:
    print("Exception occurred")
else:
    print("Everything is fine")
finally:
    print("Inside finally block")
