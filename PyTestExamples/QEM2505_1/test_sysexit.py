
# content of test_sysexit.py
import pytest

def f():
    raise SystemExit(1)

def test_mytest():
    with pytest.raises(SystemExit):
        f()

# Run the file using below command in command prompt -
# pytest -q test_sysexit.py
