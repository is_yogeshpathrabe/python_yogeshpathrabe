#! python

import sys
import locale

sys.stdout.write("Hello from Python %s\n" %(sys.version))
sys.stdout.write("Argv %s\n" %(sys.argv))
sys.stdout.write("Path %s\n" %(sys.path))
sys.stdout.write("Sys encoding used %s\n" %(sys.getfilesystemencoding()))

sys.stdout.write("Locale encoding used %s\n" %(locale.getpreferredencoding()))
