
''' Getter and Setters class to handle employee data '''

class EmployeeDataBean: 
    
    def __init__(self, name, sex, birthDate, organization): 
        self._name = name 
        self._sex = sex 
        self._birthDate = birthDate 
        self._organization = organization 
    
    # getter method 
    def getName(self): 
        return self._name 
      
    # setter method 
    def setName(self, name): 
        self._name = name
    
    # getter method 
    def getSex(self): 
        return self._sex 
      
    # setter method 
    def setSex(self, sex): 
        self._sex = sex
        
    # getter method 
    def getBirthDate(self): 
        return self._birthDate
      
    # setter method 
    def setBirthDate(self, birthDate): 
        self._birthDate = birthDate
        
    # getter method 
    def getOrganization(self): 
        return self._organization
      
    # setter method 
    def setOrganization(self, organization): 
        self._organization = organization

if __name__ == '__main__':
    
    employeeDataBean = EmployeeDataBean("Yogesh Pathrabe", "Male", "25/6/1988", "Google")
    print("Name: " + employeeDataBean.getName())
    print("Sex: " + employeeDataBean.getSex())
    print("Birth Date: " + employeeDataBean.getBirthDate())
    print("Organization: " + employeeDataBean.getOrganization())
