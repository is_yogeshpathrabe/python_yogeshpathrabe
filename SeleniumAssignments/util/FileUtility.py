
""" FileUtility.py: This file defines file utility methods. """

import os

__author__ = "Yogesh Pathrabe"

from pathlib import Path

def getProjectPath():
    return Path(__file__).parent.parent

def getResourcesPath():
    return os.path.join(getProjectPath(), 'resources')

def getServerPath():
    return os.path.join(getProjectPath(), 'server')
