# Set
""" Unordered|Unindexed|No Duplicates """
""" Syntax: {} """

setA = {"A", "B", "C", "D", "E"}
print("Main set", setA)

# Using index value we can not access set like List/Tuple

""" For loop """
print("For loop printing")
for value in setA:
    print(value)

# Validation of value in set is possible
print("\nD is present", "D" in setA)

setA.add("F")
print("\nNew set", setA)

# Adding multiple data
setA.update(["G", "H"])
print("\nNew set", setA)

print("\nSet size", len(setA))

setA.remove("C")
print("\nRemoved set", setA)

# Discard will work same as remove
# Only difference is if element not present in set then it will not give exception
# But, remove will give exception
setA.discard("C")
setA.discard("H")
print("\nDiscard set", setA)

# Remove random element
setA.pop()
print("\nPop set", setA)

setA.clear()
print("\nEmpty set", setA)

# Delete complete set
# del setA

# List type of data we can not add in set - tuple it will accept
setB = {"Infostretch", 1, 2, 3, ("Yogesh", "Pathrabe")}
print("\nNew set", setB)

# We can convert list to set
listA = ["Yogesh", "Pathrabe"]
print("\nMy list", listA)
setC = set(listA)
print("\nMy set", setC)

# UNION - Symbol |
setD = {1, 2, 3, "A", "C", "D"}
setE = {"A", "B", "F", 1, 2, 3, 4, 5}

# Combine all elements into one
print("\nUnion is", setD.union(setE)) # print("\nUnion is", setD | setE)

# INTERSECTION - Symbol &
# Give common elements
print("\nIntersection is", setD.intersection(setE)) # print("\nIntersection is", setD & setE)

#DIFFERENCE - Symbol -
# Give difference elements from first set
print("\nDifference is", setD.difference(setE)) # print("\nDifference is", setD - setE)
print("\nDifference is", setE - setD) # print("\nDifference is", setE.difference(setD))

#SYMMETRIC DIFFERENCE - Symbol ^
# Give unique elements from set, 
print("\nSymmetric difference is", setD.symmetric_difference(setE)) # print("\nSymmetric difference is", setD ^ setE)
