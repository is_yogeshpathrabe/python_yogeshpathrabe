
import os
from util.FileUtility import getResourcesPath

myFilePath = os.path.join(getResourcesPath(), 'MyFileForWrite.txt')

# Writing file - clear existing data and 'w' parameter required for write
# 'x' parameter used to create file - if file already present then it will give error
# Mode is binary / text so use 'xt' text / 'xb' binary
# 'a' for append mode
# 'r' for read mode
with open(myFilePath, 'w') as myFile:
    # Printing number of characters written
    print(myFile.write("Having 7 years of experience"))

# Reading file
with open(myFilePath) as myFile:
    print("File data:", myFile.read())

