# Class 1
class DemoClass1:
    intX = 25
    stringX = "Yogesh"
    
    def total(self, intA, intB):
        print("Total is", (intA + intB))
    
print(DemoClass1().intX)
print(DemoClass1().stringX)
DemoClass1().total(25, 88)

objectDemoClass1 = DemoClass1()
print("\n" + objectDemoClass1.stringX)
print(objectDemoClass1.intX)
objectDemoClass1.total(25, 6)

# Delete the reference of object
del objectDemoClass1

# Class 2
class DemoClass2:
    intX = 88
    """Single quote will also work"""
    firstName = 'Infostretch'
    
    """In built function which will get called while object creation"""
    def __init__(self, firstName, lastName):
        self.firstName = firstName
        self.lastName = lastName

objectDemoClass2 = DemoClass2("Yogesh","Pathrabe")
print("\n" + objectDemoClass2.firstName)
print(objectDemoClass2.lastName)
print(objectDemoClass2.intX)

del objectDemoClass2
