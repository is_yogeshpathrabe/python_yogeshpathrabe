# Tuple
""" Ordered|Indexed|Unchangeable|Duplicates """
""" Syntax: () """

tupleA = ("A", "B", "C", "D", "E")
print("Main tuple", tupleA)
print("\n2nd index value " + tupleA[2])
print("\n-2 index value " + tupleA[-2])
print("\nRange values", tupleA[1:3], "\n")

""" For loop """
print("For loop printing")
for value in tupleA:
    print(value)

# Changing the element value it not support
# Deletion of element it not support
# Entire Tuple we can delete using following -
# del tupleA

print("\nTuple size", len(tupleA), "\n")

""" Nested Tuple """
tupleC = ("Yogesh", (1, 2, 3), ['x', 'y', 'z'])
print("\nEntire tuple", tupleC)
print("\nNested tuple", tupleC[1])
print("\nNested tuple value ", tupleC[2][2])

# Changing the value for list is possible
tupleC[2][1] = "a"
print("\nEntire tuple", tupleC)

# Validation of value in tuple is possible
print("\nYogesh is present", "Yogesh" in tupleC)
print("\n(1, 2, 3) is present", (1, 2, 3) in tupleC)
print("\n2 is present", 2 in tupleC[1])
print("\nx is present", "x" in tupleC[2])
