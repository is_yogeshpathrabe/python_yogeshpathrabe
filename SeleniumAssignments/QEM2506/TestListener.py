
import unittest

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.utils import ChromeType
from selenium import webdriver
from selenium.webdriver.support.events import AbstractEventListener
from selenium.webdriver.support.events import EventFiringWebDriver


class MyListener(AbstractEventListener):

    def before_navigate_to(self, url, driver):
        print("Before navigate to %s" % url)

    def after_navigate_to(self, url, driver):
        print("After navigate to %s" % url)


class TestListener(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install())
        ef_driver = EventFiringWebDriver(self.driver, MyListener())
        ef_driver.delete_all_cookies()
        ef_driver.maximize_window()
        ef_driver.get("https://www.google.co.in/")

    def test_sample(self):
        self.assertEqual(self.driver.title, "Google")

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
