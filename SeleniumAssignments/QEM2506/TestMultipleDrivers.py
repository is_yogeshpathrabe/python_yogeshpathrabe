
import os

from util.FileUtility import getServerPath
from selenium import webdriver


class TestPractice1:

    def test_chrome(self):

        chromeServerPath = os.path.join(getServerPath(), 'chrome/chromedriver.exe')

        driver = webdriver.Chrome(executable_path=chromeServerPath)
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")
        driver.quit()

    def test_firefox(self):

        firefoxServerPath = os.path.join(getServerPath(), 'firefox/geckodriver.exe')

        driver = webdriver.Firefox(executable_path=firefoxServerPath)
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")
        driver.quit()

    def test_ie(self):

        ieServerPath = os.path.join(getServerPath(), 'internet_explorer/IEDriverServer.exe')

        driver = webdriver.Ie(executable_path=ieServerPath)
        driver.delete_all_cookies()
        driver.maximize_window()
        driver.get("https://www.google.co.in/")
        driver.quit()
