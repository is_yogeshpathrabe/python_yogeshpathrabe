"""
Print fibonacci series and take value as user input
"""

class FibonacciSeries:
    
    intFirst = 0
    intSecond = 1
        
    def usingWhileLoop(self, myInput):
        counter = 0
        while counter < myInput:
            print(self.intFirst)
            intTotal = self.intFirst + self.intSecond
            self.intFirst = self.intSecond
            self.intSecond = intTotal
            counter += 1
        
    def usingForLoop(self, myInput):
        for value in range(myInput):
            print(self.intFirst)
            intTotal = self.intFirst + self.intSecond
            self.intFirst = value
            self.intFirst = self.intSecond
            self.intSecond = intTotal
        
    def usingRecurtion(self, myInput):
        for number in range(myInput):
            print(self.getFibonacciNumber(number))

    def getFibonacciNumber(self, myNumber):
        if myNumber <= 1:
            return myNumber
        else:
            return(self.getFibonacciNumber(myNumber-1) + self.getFibonacciNumber(myNumber-2))

userInput = input("Please enter any integer number to print fibonacci series: ")
try:
    myInput = int(userInput)
    if myInput == 0:
        print("\nPlease enter value greater than 0")
    else:
        userOption = input("\nPlease select any one option to print fibonacci series - \n \
        1 Using while loop\n \
        2 Using for loop\n \
        3 Using recursion\n")
        try:
            myOption = int(userOption)
            if myOption == 1:
                print("\nFibonacci series using while loop is ")
                FibonacciSeries().usingWhileLoop(myInput)
            elif myOption == 2:
                print("\nFibonacci series using for loop is ")
                FibonacciSeries().usingForLoop(myInput)
            elif myOption == 3:
                print("\nFibonacci series using recursion is ")
                FibonacciSeries().usingRecurtion(myInput)
            else:
                print(myOption, "is invalid option")
        except:
            print(userOption, "is invalid option")
except:
    print(userInput, "is invalid integer number")
