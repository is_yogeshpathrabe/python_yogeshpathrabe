
import os
from util.FileUtility import getResourcesPath

directoryName = os.path.dirname(__file__)
# It will print path of current package
# print(directoryName)

myFilePath = os.path.join(getResourcesPath(), 'MyFile.txt')
# It will print path of file
# print(myFilePath)

# Reading file
# myFile = open(myFilePath, 'r')
# Using with will automatically close the file after doing required operation
with open(myFilePath) as myFile:
    # print('Data from file:\n' + myFile.read())
    # print('Data from file:\n' + myFile.read())
    myData = myFile.read()

# Printing file data
# print(myFile.read())
print('Data from file:\n' + myData)

# Use to close file
#myFile.close()
print("\nFile closed:", myFile.closed)

# Readline example - will read file line by line
with open(myFilePath) as myFile:
    print("\nReadline reading:", myFile.readline())
    print("Readline reading:", myFile.readline())

# For loop reading
print("\nFor loop reading:")
with open(myFilePath) as myFile:
    for line in myFile:
        print(line, end='')

# Creating and deleting the file
newFilePath = getResourcesPath() + "/DemoFile.txt"
newFile = open(newFilePath, "xt")
newFile.close()

if(os.path.exists(newFilePath)):
    os.remove(newFilePath)
    print("\n" + newFilePath + " file deleted successfully")
else:
    print("\n" + newFilePath + " file is not available")
