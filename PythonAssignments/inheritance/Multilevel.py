
# Grand parent class
class GrandParent:
    
    def family(self):
        print("I am grand parent")
        
# Parent class
class Father(GrandParent):
    
    fatherName = ""
    
    def father(self):
        print("My name is " + self.fatherName)
    
# Child class
class Son(Father):
    
    sonName = ""
    
    def son(self):
        print("My father is " + self.fatherName)

sonObject = Son()
sonObject.fatherName = "Atmaram"

sonObject.family()
sonObject.son()
sonObject.father()
