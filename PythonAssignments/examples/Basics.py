
myString = "Hello World..."
print(myString)

print()
# Variables
intX = 10
intY = 10

print(intX * intY)

intY = 20

print()
# If usage
if intX == intY:
    print(str(intX) + " equals " + str(intY))
else:
    print(str(intX) + " not equals " + str(intY))

print()
# String functions
stringX = "  Infostretch  "
print("Value without trim: " + stringX)
print("Value with trim: " + stringX.strip())

print(myString[3])
print(myString[0:5])
print(len(myString))
print(myString.replace("W", "w"))
print(myString.upper())

print()
# Casting
castW = int(2)
castX = int(2.5)
castY = float(1.1)
castZ = str(25)

print(castW)
print(castX)
print(castY)
print(castZ)

print()
# Variable types
print(type(castX))
print(type(castY))
print(type(castZ))
