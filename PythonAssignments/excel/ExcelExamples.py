
import openpyxl
import os
from util.FileUtility import getResourcesPath

myFilePath = os.path.join(getResourcesPath(), 'TrackingDetails.xlsx')

# Getting excel file object
workbook = openpyxl.load_workbook(myFilePath)
print("Type of workbook: " + str(type(workbook)))

# To print the list of sheets
print("\nList of sheets: " + str(workbook.sheetnames))

# To get sheet using sheet name
employeesSheet = workbook['Employees']
print("\nSelected sheet: " + str(employeesSheet))
print("Type of sheet: " + str(type(employeesSheet)))
print("Name of sheet: " + str(employeesSheet.title))

# To print the active sheet name
print("\nActive sheet: " + str(workbook.active))

# To read the data from sheet - column name and row number example, A1, F2, etc.
print("\nEmployee name: " + employeesSheet['B2'].value)

cell = employeesSheet['F2']
print('\nRow ' + str(cell.row) + ', Column ' + str(cell.column) + ' is ' + cell.value)
print('Cell ' + cell.coordinate + ' is ' + cell.value)

""" Using index numbers """
cell = employeesSheet.cell(row=2, column=2)
print('\nRow ' + str(cell.row) + ', Column ' + str(cell.column) + ' is ' + cell.value)
print('Cell ' + cell.coordinate + ' is ' + cell.value)

print("\nEmployee names (after 1st showing 3rd name): ")
for index in range(2, 17, 3):
        print(index, employeesSheet.cell(row=index, column=2).value)
        
print("\nMaximum rows are " + str(employeesSheet.max_row))
print("Maximum columns are " + str(employeesSheet.max_column))
