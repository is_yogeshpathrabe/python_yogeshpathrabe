
import os
from util.FileUtility import getResourcesPath
from excel.ExcelUtilty import ExcelUtility
from QEM2504.EmployeeDataBean import EmployeeDataBean
from QEM2504.OrganizationDataBean import OrganizationDataBean

trackingDetailsFilePath = os.path.join(getResourcesPath(), 'TrackingDetails.xlsx')

# EmployeesTrackingSystem
class EmployeesTrackingSystem:
    
    _employeeDataBean = ''
    
    def __init__(self, employeeDataBean): 
        self._employeeDataBean = employeeDataBean

    def getEmployeeDataBean(self): 
        return self._employeeDataBean 
    
    employeesData = ExcelUtility().getFile(trackingDetailsFilePath, "Employees")
    
    def validateEmployee(self):
        found = ExcelUtility().isEmployeePresent(self.employeesData, self.getEmployeeDataBean().getName(), self.getEmployeeDataBean().getOrganization())
        if found != -1:
            if self.employeesData['Name'][found] == self.getEmployeeDataBean().getName() and\
            self.employeesData['Sex'][found] == self.getEmployeeDataBean().getSex() and\
            self.employeesData['Organization Name'][found] == self.getEmployeeDataBean().getOrganization() and\
            self.employeesData['Birth Date'][found] == self.getEmployeeDataBean().getBirthDate():
                print("\n'" + self.getEmployeeDataBean().getName() + "' with Organization '" + self.getEmployeeDataBean().getOrganization() + "' is the same Employee.")
            else:
                print("\n'" + self.getEmployeeDataBean().getName() + "' with Organization '" + self.getEmployeeDataBean().getOrganization() + "' is the different Employee.")
        else:
            print("\nEmployee '" + self.getEmployeeDataBean().getName() + "' with Organization '" + self.getEmployeeDataBean().getOrganization() + "' is not present in the records.")
            print("This means '" + self.getEmployeeDataBean().getName() + "' is the new employee here.")
        
# OrganizationsTrackingSystem
class OrganizationsTrackingSystem:
    
    _organizationDataBean = ''
    
    def __init__(self, organizationDataBean): 
        self._organizationDataBean = organizationDataBean

    def getOrganizationDataBean(self): 
        return self._organizationDataBean 
    
    organizationsData = ExcelUtility().getFile(trackingDetailsFilePath, "Organizations")
    
    def validateOrganization(self):
        found = ExcelUtility().isOrganizationPresent(self.organizationsData, self.getOrganizationDataBean().getOrganization())
        if found != -1:
            if self.organizationsData['Organization Name'][found] == self.getOrganizationDataBean().getOrganization() and\
            self.organizationsData['Number of Employees'][found] == self.getOrganizationDataBean().getNumberOfEmployees():
                print("\n'" + self.getOrganizationDataBean().getOrganization() + "' is the same Organization.")
            else:
                print("\n'" + self.getOrganizationDataBean().getOrganization() + "' is the different Organization.")
        else:
            print("\nOrganization '" + self.getOrganizationDataBean().getOrganization() + "' is not present in the records.")
            print("This means '" + self.getOrganizationDataBean().getOrganization() + "' is the new organization here.")

# Main method
if __name__ == '__main__':
    
    # Validating employee detail
    print("\nEmployee Detail Tracking")
    # employeeName = input("Please enter employee name: ")
    # sex = input("Please enter sex: ")
    # birtDate = input("Please enter birth date: ")
    # organizationName = input("Please enter organization name: ")
    # employeeDataBean = EmployeeDataBean(employeeName, sex, birtDate, organizationName)
    employeeDataBean = EmployeeDataBean("Yogesh Pathrabe", "Male", "25/6/1988", "Microsoft")
    employeesTrackingSystem = EmployeesTrackingSystem(employeeDataBean)
    employeesTrackingSystem.validateEmployee()
    
    # Validating organization detail
    print("\nOrganization Detail Tracking")
    # organizationName = input("Please enter organization name: ")
    # numberOfEmployees = input("Please enter number of employees: ")
    #organizationDataBean = OrganizationDataBean(organizationName, numberOfEmployees)
    organizationDataBean = OrganizationDataBean("Infostretch", 500)
    organizationsTrackingSystem = OrganizationsTrackingSystem(organizationDataBean)
    organizationsTrackingSystem.validateOrganization()
