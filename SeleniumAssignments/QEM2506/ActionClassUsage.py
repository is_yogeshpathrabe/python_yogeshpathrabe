
import time
import os

from util.FileUtility import getServerPath
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By

''' Do not run code - it's just an example '''


class TestActionClassUsage:

    def test_action_class_usage(self):

        chromeServerPath = os.path.join(getServerPath(), 'chrome/chromedriver.exe')

        driver = webdriver.Chrome(executable_path=chromeServerPath)
        driver.get("https://test.snowflakecomputing.com/console/login#/")

        try:
            wait = WebDriverWait(driver, 60)
            search_bar = wait.until(ec.presence_of_element_located((By.XPATH, "//div[@class='CodeMirror-code']")))

            search_bar.click()

            # Create the object for Action Chains
            actions = ActionChains(driver)
            actions.move_to_element(search_bar)
            actions.click()
            actions.send_keys("select *")
            actions.perform()

            # Used to pause code - Don't use in projects
            time.sleep(10)
        finally:
            driver.quit()
