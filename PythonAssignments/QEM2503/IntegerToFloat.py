"""
Converting integer to float
"""

myInteger = 12
print("\nInteger value is", myInteger, "and the data type is", type(myInteger))

myFloat = float(myInteger)
print("\nFloat value is", myFloat, "and the data type is", type(myFloat))
