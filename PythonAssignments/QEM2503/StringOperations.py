"""
input = "GameOfThrones"
5 outputs:
a) extract from input => "one"
b) convert (a) from "one" to => "One"
c) reverse the input => "senorhTfOemaG"
d) reverse the input case => "gAMEoFtHRONES"
e) extract caps into one string => "GOT"
"""

myInput = "GameOfThrones"
print("input = \"" + myInput + "\"")

#a
print("\na) extract from input => \"one\"")
aInput = myInput[9:12]
print("Output: " + aInput)

#b
print("\nb) convert (a) from \"one\" to => \"One\"")
print("Output: " + aInput.capitalize())

#c
print("\nc) reverse the input => \"senorhTfOemaG\"")
def doReverse(anyInput):
    splitInput = [char for char in anyInput]
    splitInput.reverse()
    reverseInput = ""
    for charecter in splitInput:
        reverseInput = reverseInput + charecter
    return reverseInput
print("Output: " + doReverse(myInput))

#d
print("\nd) reverse the input case => \"gAMEoFtHRONES\"")
print("Output: " + myInput.swapcase())

#e
print("\ne) extract caps into one string => \"GOT\"")
def getCapitalLetters(anyInput):
    capitalLetter = ''
    for letter in anyInput:
        if str(letter).isupper():
            capitalLetter = capitalLetter + letter
    return capitalLetter
print("Output: " + getCapitalLetters(myInput))
