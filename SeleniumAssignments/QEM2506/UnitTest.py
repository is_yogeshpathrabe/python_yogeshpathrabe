
import unittest

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.utils import ChromeType
from selenium import webdriver


class UnitTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install())
        self.driver.delete_all_cookies()
        self.driver.maximize_window()
        self.driver.get("https://www.google.co.in/")

    def test_sample(self):
        self.assertEqual(self.driver.title, "Google")

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
