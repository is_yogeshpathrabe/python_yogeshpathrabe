
''' Getter and Setters class to handle organization data '''

class OrganizationDataBean: 
    
    def __init__(self, organization, numberOfEmployees): 
        self._organization = organization
        self._numberOfEmployees = numberOfEmployees
    
    # getter method 
    def getOrganization(self): 
        return self._organization
      
    # setter method 
    def setOrganization(self, organization): 
        self._organization = organization
        
    # getter method 
    def getNumberOfEmployees(self): 
        return self._numberOfEmployees
      
    # setter method 
    def setNumberOfEmployees(self, numberOfEmployees): 
        self._numberOfEmployees = numberOfEmployees

if __name__ == '__main__':
    
    organizationDataBean = OrganizationDataBean("Infostretch", 500)
    print("Organization: " + organizationDataBean.getOrganization())
    print("Number of Employees: " + str(organizationDataBean.getNumberOfEmployees()))
