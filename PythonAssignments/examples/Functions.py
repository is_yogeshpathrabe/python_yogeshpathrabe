# Variables
intX = 10
intY = 10

# Functions
def doSum(intX, intY):
    return intX + intY

sumXY = doSum(intX, intY)
print("Total is " + str(sumXY))

def doDefaultSum(intX = 20, intY = 50):
    """This method will return sum of provide values"""
    return intX + intY

print("\nTotal is", doDefaultSum(15, 25))
print("Default total is", doDefaultSum())
